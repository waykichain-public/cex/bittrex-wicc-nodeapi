## Configuration Information
The following configuration information must be provided for proper integration of your token software with the Bittrex platform. <br>
The following configuration settings (CS#) should be provided to the Bittrex Coin Team as part of the integration process.

## Q&A

#| Question | Answer
--- | --- | ---
|CS1|Does the token support multiple assets? | Yes. WaykiChain is implemented with an extensive account model and currently supports the base coins (WICC) and governance coins (WGRT). Any WICC coin holder can use WaykiChain CDP to mint or generate stablecoins called WUSD which is 1:1 soft pegged to US dollar in terms of its holding value.|
|CS2|Does the token software support multiple outputs per transaction?|It depends on the type of transactions. We have receipt field in the output and there can be multiple such receipts.|
|CS3|Are transaction costs paid as gas or fees?| Most Transactions are paid as fees. But for contract related transactions, gas deduction will be also involved.|
| CS3.1|Is gas/fee specified per-transaction? Or can it be configured on the node software (e.g. as a cap or limit)?| There's a minimum fee for each type of transaction. Pls refer to [WICC Fees Table](https://wicc-devbook.readthedocs.io/en/latest/Problem/fees/) |
| CS3.2| If paid in gas, can the total gas per transaction be estimated?| Yes. For contract related trasactions, we will provide a RPC method to estimate the gas. |
| CS3.3| Is gas paid in a different token from the main asset?| Gas can be paid in either WICC or WUSD coins|
| CS4| Can the wallet be encrypted/locked?| Yes. Encryption/locking is supported.|
|   CS4.1| When unlocking, can the client specify a timeout?| Yes. Timeout setting is available. |
|CS5| Does a transaction require a nonce?| Similarily, to avoid sending out duplicated transaction, it's required that each transaction must be composed with a field named as "valid height" which is the current tip block height (with 250 deviation accepted) |
|CS6| Does transaction signing require a secret?| Nope |
|CS7| Does your wallet use HD keys?| Yes |