const express = require('express');
const router = express.Router();
const { spawn } = require('child_process');


// @route         GET wallet/address
// @description   creates a new address
// @access        Public

router.post('/', (request, response) => {

    const getinfo = spawn('coind', ['getnewaddr']);

    getinfo.stdout.on('data', (data) => {
        response.json(JSON.parse(`${data}`));
        console.log(`${data}`);

        getinfo.stderr.on('data', (data) => {
            response.json(JSON.parse(`${data}`));
            console.error(`stderr: ${data}`);
        });
    });
});

module.exports = router;
