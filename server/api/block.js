const express = require('express');
const router = express.Router();
const { spawn } = require('child_process');

// @route         GET node/block/:height
// @description   get node block by height
// @access        Public

router.get('/:height', (request, response) => {

    let height = request.params.height;
    const getblock =  spawn('coind', ['getblock', height]);
    getblock.stdout.on('data', (data) => {
        let blockJson = JSON.parse(`${data}`);

        // iterate through all transactions inside the block
        const promiseArray = blockJson.tx.map((item, i) => {
            const gettxdetail = spawn('coind', ['gettxdetail', item]);
            return new Promise((resolve, reject) => {
                gettxdetail.stdout.on('data', (data) => {
                    let txDetailJson = JSON.parse(`${data}`);

                    if (txDetailJson.tx_type === 'UCOIN_TRANSFER_TX') {

                        for (let i = 0; i < txDetailJson.transfers.length; i++) {
                            let transferDetailJson = txDetailJson.transfers[i];
                            let transfer = {
                                "hash": txDetailJson.txid,
                                "payments": [
                                    {
                                        "from": txDetailJson.from_addr,
                                        "to": transferDetailJson.to_addr,
                                        "amount": transferDetailJson.coin_amount
                                    },
                                ]
                            };
                            resolve(transfer)
                        }
                    } else {
                        resolve(false)
                    }
                })
            })
        });

        Promise.all(promiseArray).then(array => {
            let transactions = array.filter(item => {
                return item;
            });

            response.json({
                "hash": blockJson.block_hash,
                "height": blockJson.height,
                "transactions": transactions
            })
        })
    });

    getblock.stderr.on('data', (data) => {
        response.status(404).json("HTTP 404")
    });
});

module.exports = router;
