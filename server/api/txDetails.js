const express = require('express');
const router = express.Router();
const { spawn } = require('child_process');


// @route         GET node/transaction/:hash
// @description   get node transaction information with hash
// @access        Public

router.get('/:hash', (request, response) => {

    let hash = request.params.hash;

    const gettxdetail = spawn('coind', ['gettxdetail', hash]);

    gettxdetail.stdout.on('data', (data) => {
        let txDetailJson = JSON.parse(`${data}`);

        if (txDetailJson.tx_type === "UCOIN_TRANSFER_TX") {

            let payments = [];
            for (let i = 0; i < txDetailJson.transfers.length; i++) {
                let transferDetailJson = txDetailJson.transfers[i];
                let transfer = {
                    "from": txDetailJson.from_addr,
                    "to": transferDetailJson.to_addr,
                    "amount": transferDetailJson.coin_amount
                };
                payments.push(transfer);
            }

            response.json({
                "hash": txDetailJson.txid,
                "blockHash": txDetailJson.block_hash,
                "blockHeight": txDetailJson.confirmed_height,
                "payments": payments
            });
        }else {
                response.status(404).send({error: "HTTP 404"})
            }
        });

        gettxdetail.stderr.on('data', (data) => {

        response.status(404).json("HTTP 404")
    });
});

module.exports = router;
