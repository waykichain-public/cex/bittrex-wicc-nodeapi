const express = require('express');
const router = express.Router();
const { spawn } = require('child_process');


// @route         POST node/transaction/:submittxraw
// @description   submit a raw transaction with a raw tx
// @access        Public

router.post('/', (request, response) => {

    let submittxraw = request.body.data;

    const submittxrawreq = spawn('coind', ["submittxraw", submittxraw]);
    submittxrawreq.stdout.on('data', (data) => {

        let resData = JSON.parse(`${data}`);

        response.status(200).json(
            {
                "hash": resData.txid,
                "costEstimate": null
            });
    });
    submittxrawreq.stderr.on('data', (data) => {

        let errJson = `${data}`;
        let rmStr = errJson.length -7;
        let errCode = JSON.parse(errJson.substr(7, rmStr));
        let errMessage = JSON.parse(errJson.substr(7, rmStr));
        console.log(`${data}`);

        response.status(400).json({

            "errorCode": errCode.code,
                "errorMessage": errMessage.message
            })
    });
});


module.exports = router;
