'use strict';
const express = require('express');

//api paths
const nodeInfo = require('./api/info');
const blockHeight = require('./api/block');
const checkAddr = require('./api/checkAddr');
const txDetails = require('./api/txDetails');
const transferTx = require('./api/transferTx');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();

app.use(express.urlencoded({extended: true}));
app.use(express.json());


// Use the api routes
app.use('/node/info', nodeInfo);
app.use('/node/block', blockHeight);
app.use('/node/transaction', txDetails);
app.use('/node/address', checkAddr);
app.use('/node/transaction', transferTx);

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
